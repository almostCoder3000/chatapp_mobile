import React, { ComponentState } from 'react';
import { 
    View, Text, TextInput, KeyboardAvoidingView, AsyncStorage, WebView, NavState 
} from 'react-native';
import Animated from 'react-native-reanimated';
import { connect } from "react-redux";
import {TapGestureHandler, TouchableOpacity} from 'react-native-gesture-handler';
import Svg, {Image, Circle, ClipPath} from 'react-native-svg';
import { height, width, styles } from './styles';
import AnimateBaseClass, {concat} from "./animation";
import { AppState } from '../../store';
import { ThunkDispatch } from 'redux-thunk';
import { logIn, vkLogIn } from '../../store/user/actions';
import { NavigationStackProp } from 'react-navigation-stack';
import { UserState } from '../../store/user/types';
import configure from '../../configure';

interface state {
    login: string;
    password: string;
    isVkAuth: boolean;
}
interface props {
    logIn: (login:string, password: string) => void;
    vkLogIn: (code:string) => void;
    navigation: NavigationStackProp;
    user: UserState;
}


class LoginScreen extends AnimateBaseClass<props, state> {
    state = {
        login: "",
        password: "",
        isVkAuth: false
    }
    componentWillMount() {
        this._checkToken();
    }
    componentDidUpdate() {
        this._checkToken();
    }

    async _checkToken():Promise<void> {
        const val:string = await AsyncStorage.getItem("token");
        if (val) this.props.navigation.navigate("HomeStack"); 
    }

    onChangeInputHandler(field:string, text:string) {
        this.setState({[field]: text} as ComponentState)
    }

    logInAction() {
        this.props.logIn(this.state.login, this.state.password);        
    }

    vkAuthAction() {
        this.setState({isVkAuth: true});
    }

    _setVkToken(e:NavState) {
        //AsyncStorage.setItem("vk_token", e.url.split("access_token=")[1].split("&")[0]);      
        //this.props.navigation.navigate("HomeStack");
        const code:string|undefined = e.url.split("code=")[1];

        if (code) {
            this.props.vkLogIn(code);
            this.setState({isVkAuth: false});
            this.props.navigation.navigate("HomeStack");
        }
    }

    signUpAction() {
        this.props.navigation.push("SignUpScreen")
    }

    render() {      
        if (this.state.isVkAuth) {
            return (
                <WebView 
                    style={{marginTop: 40}}
                    onNavigationStateChange={this._setVkToken.bind(this)}
                    source={{uri: `https://oauth.vk.com/authorize?client_id=${configure.oauth.client_id}&scope=email`}} />
            )
        }
        return (
            <KeyboardAvoidingView behavior="padding" enabled style={styles.container}>
                {/* Фон полукругом */}
                <Animated.View style={{
                        ...styles.absoluteFill,
                        transform: [{translateY: this.bgY}]
                    }}>
                    <Svg height={height + 55} width={width + 2}>
                        <ClipPath id="clip">
                            <Circle r={height + 55} cx={width/2} />
                        </ClipPath>
                        <Image 
                            href={require('../../../assets/bg1.jpg')}
                            width={width + 2}
                            height={height + 55}
                            preserveAspectRatio='xMidYMid slice'
                            clipPath="url(#clip)"
                        />
                    </Svg>
                </Animated.View>
                
                {/* Кнопки входа, реги и ВК с формой входа и регистрации */}
                <View style={{height: height/3 }}>
                    
                    {/* Кнопка ВОЙТИ для анимации */}
                    <TapGestureHandler onHandlerStateChange={this.onStateChange}>
                        <Animated.View style={{
                                ...styles.button, 
                                opacity: this.buttonOpacity,
                                transform: [{translateY: this.buttonY}]
                            }}>
                            <Text style={{fontSize: 20, fontWeight: 'bold'}}>Войти</Text>
                        </Animated.View>
                    </TapGestureHandler>
                    
                    {/* Кнопка авторизации через вк и кнопка для анимации регистрации */}
                    <View style={{flexDirection: "row", justifyContent: "space-between", marginHorizontal: 40}}>
                        <Animated.View style={{
                                ...styles.smallButton, 
                                width: "49%", 
                                backgroundColor: '#4c75a3',
                                opacity: this.buttonOpacity,
                                transform: [{translateY: this.buttonY}]
                            }}
                            onTouchStart={this.vkAuthAction.bind(this)}>
                            <Text style={{fontSize: 20, fontWeight: 'bold', color: 'white'}}>VK</Text>
                        </Animated.View>

                        
                        <Animated.View style={{
                                ...styles.smallButton, 
                                width: "49%", 
                                backgroundColor: "#fff",
                                opacity: this.buttonOpacity,
                                transform: [{translateY: this.buttonY}]
                            }}>
                            <TouchableOpacity style={{
                                width: "100%",
                                alignItems: 'center',
                                justifyContent: 'center', 
                                height: "100%"
                            }} onPress={this.signUpAction.bind(this)}>
                                    <Text style={{fontSize: 20, fontWeight: 'bold', color: '#000'}}>Регистрация</Text>
                            </TouchableOpacity>
                        </Animated.View>
                    </View>
                    
                    {/* Выплывающая форма входа */}
                    <Animated.View style={{
                            ...styles.absoluteFill, 
                            height: height/3,
                            top: 0,
                            justifyContent: 'center',
                            zIndex: this.textInputZindex,
                            opacity: this.textInputOpacity,
                            transform: [{translateY: this.textInputY}]
                        }}>
                        <TapGestureHandler onHandlerStateChange={this.onCloseState}>
                            <Animated.View style={styles.closeButton}>
                                <Animated.Text style={{
                                        fontSize: 15,
                                        transform: [{rotate: concat(this.rotateCross, 'deg')}]
                                    }}>X</Animated.Text>
                            </Animated.View>
                        </TapGestureHandler>
                        <TextInput
                            placeholder="Логин"
                            style={styles.textInput}
                            placeholderTextColor="black"
                            onChangeText={this.onChangeInputHandler.bind(this, "login")}
                        />
                        <TextInput
                            placeholder="Пароль"
                            secureTextEntry={true}
                            style={styles.textInput}
                            placeholderTextColor="black"
                            onChangeText={this.onChangeInputHandler.bind(this, "password")}
                        />
                        <Animated.View style={{...styles.button, marginTop: 10}} onTouchStart={this.logInAction.bind(this)}>
                            <Text style={{fontSize: 20, fontWeight: 'bold', color: "#232323"}}>Войти</Text>
                        </Animated.View>
                    </Animated.View>
                </View>
            </KeyboardAvoidingView>
        );
    }
}


const mapStateToProps = (state: AppState) => ({
    user: state.user
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    logIn: async (login: string, password:string) => {
        dispatch(logIn(login, password));
    },
    vkLogIn: async (code:string) => {
        dispatch(vkLogIn(code));
    },
    
})


export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
