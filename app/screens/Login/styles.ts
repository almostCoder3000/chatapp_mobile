import {StyleSheet, Dimensions} from 'react-native';

export const {width, height} = Dimensions.get('window');

export const styles = StyleSheet.create({
    container: {
        flex: 1, 
        backgroundColor: "white", 
        justifyContent: 'flex-end'
    },
    closeButton: {
        height: 40,
        width: 40,
        backgroundColor: 'white',
        borderRadius: 20,
        alignItems: "center",
        justifyContent: 'center',
        position: 'absolute',
        top: -20,
        left: width/2 - 20,
        elevation: 2
    },
    textInput: {
        height: 50,
        borderBottomWidth: 0.5,
        marginHorizontal: 40,
        borderRadius: 5,
        paddingLeft: 10,
        marginVertical: 7,
        borderColor: 'rgba(0,0,0,0.2)',
        backgroundColor: "#ffffff",
        elevation: 2
    },
    button: {
        backgroundColor: 'white',
        height: 60,
        marginHorizontal: 40,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 5,
        elevation: 2
    },
    smallButton: {
        height: 60,
        borderRadius: 5,
        alignItems: 'center',
        justifyContent: 'center',
        marginVertical: 5,
        elevation: 2
        
    },
    absoluteFill: {
        position: "absolute",
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
    }
});