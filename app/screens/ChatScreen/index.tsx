import React, { Component, ComponentState } from 'react';
import {
     View, Text, TextInput, KeyboardAvoidingView, ScrollView, 
     YellowBox, AsyncStorage , Image
    } from 'react-native';
import {styles} from "./styles";
import { Entypo, FontAwesome, AntDesign } from '@expo/vector-icons';
import AnimatedButton from '../../components/AnimatedButton';
import { NavigationStackProp } from 'react-navigation-stack';
import HeaderChat from '../../components/HeaderChat';
import { IUser } from '../../store/user/types';
import { AppState } from '../../store';
import { ThunkDispatch } from 'redux-thunk';
import { getMessages, sendMessage, clearChat, setMessageSocket, setNewMessageSocket } from '../../store/chat/actions';
import { connect } from 'react-redux';
import { ChatState, IMessage } from '../../store/chat/types';
import socketIOClient from 'socket.io-client';
import configure from '../../configure';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';


YellowBox.ignoreWarnings(['Unrecognized WebSocket connection option(s) `agent`, `perMessageDeflate`, `pfx`, `key`, `passphrase`, `cert`, `ca`, `ciphers`, `rejectUnauthorized`. Did you mean to put these under `headers`?']);

interface props {
    navigation: NavigationStackProp;
    getMessages: (addressee_id:string) => void;
    sendMessage: (message:string, addressee_id:string) => void;
    clearChat: () => void;
    setMessagesSocket: (messages:IMessage[]) => void;
    setNewMessageSocket: (message: IMessage) => void;
    chat: ChatState;
    me: IUser;
}

interface state {
    message: string;
    uploadPhotoUri: string;
}

class ChatScreen extends Component<props, state> {
    static navigationOptions = {
        header: null
    }

    scroll:ScrollView;
    socket:SocketIOClient.Socket;

    state = {
        message: "",
        uploadPhotoUri: ""
    }

    componentDidMount() {
        AsyncStorage.getItem("token").then((token) => {
            this.socket = socketIOClient(configure.urlServer, {
                forceNew: true,
                query: `token=${token}`
            });
            //this.props.getMessages(this.props.navigation.state.params.addressee._id);
            this.socket.emit('join', this.props.me._id, this.props.navigation.state.params.addressee._id);
            this.socket.on('chat', (msg) => { this.props.setNewMessageSocket(msg) });
            this.socket.on('private', (msgs) => { this.props.setMessagesSocket(msgs) });
        });
    }

    componentWillUnmount() {
        this.props.clearChat();
    }

    _renderMessages() {
        return this.props.chat.current_chat.messages.map((message:IMessage, ind) => {
            let styleMe, styleContact;
            if (message.from === this.props.me._id) {
                styleMe = {
                    container: {
                        justifyContent: "flex-end"
                    },
                    box: {
                        backgroundColor: "#fff"
                    },
                    text: {
                        color: "#3A404A", fontSize: 16
                    }
                }
            } else {
                styleMe = {
                    container: {
                        justifyContent: "flex-start"
                    },
                    box: {
                        backgroundColor: "#0078FF"
                    },
                    text: {
                        color: "#fff", fontSize: 16
                    }
                }
            }
            let time:string[] = (new Date(message.date)).toLocaleTimeString().split(":");
            time.pop();
            return (
                <View style={{
                    ...styles.messageContainer,
                    ...styleMe.container
                }} key={ind}>
                    <View style={{
                        ...styles.messageBox,
                        ...styleMe.box
                    }}>
                        <Text style={styleMe.text}>{message.text}</Text>
                        <Text style={{
                            ...styles.messageTime
                        }}>{time.join(":")}</Text>
                    </View>
                </View>
            )
        })
    }

    onChangeInputHandler(field:string, text:string) {
        this.setState({[field]: text} as ComponentState)
    }

    sendMessage() {
        this.socket.emit("chat", this.state.message);
        this.setState({message: ""});
    }

    async attachPress() {
        await Permissions.askAsync(Permissions.CAMERA_ROLL);
        const res = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.Images
        });

        if (res.cancelled === false) {
            this.setState({uploadPhotoUri: res.uri});
        }
    }

    _renderMediaPanel() {
        if (this.state.uploadPhotoUri.length > 0) {
            return (
                <View style={styles.mediaPanel}>
                    <View style={styles.photoContainer}>
                        <Image 
                            style={styles.photoMediaPanel} 
                            source={{uri: this.state.uploadPhotoUri}} 
                        />
                        <View style={{position: "absolute", top: -5, right: -5, zIndex: 5}}>
                            <TouchableWithoutFeedback onPress={() => {this.setState({uploadPhotoUri: ""})}}>
                                <View style={styles.closePhotoBtn}>
                                    <AntDesign name="close" color="#fff" size={16} />
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </View>
                </View>
            )
        }
    }

    render() {         
        let addressee:IUser = this.props.navigation.state.params.addressee;
        return (
            <KeyboardAvoidingView behavior="height" enabled  style={styles.container}>
                <HeaderChat isChat={true} pressLogout={() => {}}>
                    <View style={styles.photo}>
                        <Text style={styles.textPhoto}>{addressee.secondName[0]}{addressee.firstName[0]}</Text>
                    </View>
                    <View style={styles.headerNameContainer}>
                        <Text style={styles.headerName}>{addressee.secondName} {addressee.firstName}</Text>
                        <Text style={styles.headerNameStatus}>Онлайн?</Text>
                    </View>
                </HeaderChat>
                <ScrollView 
                        ref={(node) => {this.scroll = node;}} 
                        style={{marginBottom: 50}}
                        onContentSizeChange={(widthScreen, heightScreen) => {
                            this.scroll.scrollToEnd();
                        }}
                    >
                    <View style={{paddingVertical: 15}}>
                        {this._renderMessages.bind(this)()}
                    </View>
                </ScrollView>
                
                {
                    this._renderMediaPanel.bind(this)()
                }
                    
                <View style={styles.typingPanel}>
                    <AnimatedButton 
                        title={(<Entypo name="attachment" color="#AEBFCE" size={24} />)} 
                        type="text"
                        rippleColor="#0078FF"
                        onPress={this.attachPress.bind(this)} />
                    <TextInput 
                        multiline={true}
                        value={this.state.message}
                        onChangeText={this.onChangeInputHandler.bind(this, "message")}
                        numberOfLines={4}
                        style={styles.typingInput}
                        placeholder="Пора писать..." />
                    
                    <AnimatedButton 
                        title={(
                            <FontAwesome 
                                name="send" 
                                color={this.state.message === "" ? "#AEBFCE" : "#0078FF"}
                                size={24} />
                        )} 
                        type="text"
                        rippleColor="#0078FF"
                        onPress={this.sendMessage.bind(this)} />
                </View>
            </KeyboardAvoidingView>
        );
    }
}



const mapStateToProps = (state: AppState) => ({
    chat: state.chat,
    me: state.user.me
});

const mapDispatchToProps = (dispatch:ThunkDispatch<{}, {}, any>) => ({
    getMessages: async (addressee_id:string) => {
        dispatch(getMessages(addressee_id));
    },
    setMessagesSocket: async (messages:IMessage[]) => {
        dispatch(setMessageSocket(messages));
    },
    sendMessage: async (message:string, addressee_id:string) => {
        dispatch(sendMessage(message, addressee_id));
    },
    setNewMessageSocket: async (message: IMessage) => {
        dispatch(setNewMessageSocket(message));
    },
    clearChat: async () => {
        dispatch(clearChat());
    }
})


export default connect(mapStateToProps, mapDispatchToProps)(ChatScreen);
