import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#F1F1F3",
        flexDirection: "column",
        
    },
    headerText: {
        fontSize: 22, 
        color: "#4B4B50",
        fontWeight: "bold"

    }
});