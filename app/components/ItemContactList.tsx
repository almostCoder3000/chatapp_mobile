import React from 'react';
import { 
    View, Text, StyleSheet, TouchableWithoutFeedback 
} from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';



type Props = {
    firstName: string;
    secondName: string;
    photo?: string;
    isActive: boolean;
    nickname: string;
    onPress: () => void;
}

const ItemContactList:React.SFC<Props> = (props: Props) => {
    function getRandomColor():string {
        var letters = '0123456789ABCDEF';
        var color = '#00';
        for (var i = 0; i < 4; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        //color += "FF";
        return color;
    }

    return (
        <TouchableWithoutFeedback onPress={props.onPress}>
            <View style={styles.container}>
                <View style={{
                        ...styles.photo,
                        //backgroundColor: getRandomColor()
                    }}>
                    <Text style={styles.photoText}>{props.secondName[0]}{props.firstName[0]}</Text>
                    <View style={styles.status}></View>
                </View>
                <View style={styles.content}>
                    <View style={{flexDirection: "column"}}>
                        <Text style={styles.mainText}>{props.secondName} {props.firstName}</Text>
                        <Text style={styles.subText}>{props.nickname}</Text>
                    </View>
                    <View>
                        <MaterialCommunityIcons name="pencil" color="#0078FF" size={24} />
                    </View>
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 20,
        paddingVertical: 20,
        borderBottomColor: "#c4c3cb",
        borderBottomWidth: .5,
        flexDirection: "row"
    },
    photo: {
        width: 60,
        height: 60,
        borderRadius: 60,
        backgroundColor: "#c4c3cb",
        position: "relative",
        marginRight: 20,
        justifyContent: "center",
        alignItems: "center",
        elevation: 1
    },
    photoText: {
        color: "#fff",
        fontSize: 22,
        fontWeight: "100"
    },
    status: {

    },
    content: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        flexGrow: 1
    },
    mainText: {
        fontSize: 18,
        color: "#3A404A",
        fontWeight: "500"
    },
    subText: {
        fontSize: 15,
        color: "#c4c3cb"
    }
});

export default ItemContactList;
