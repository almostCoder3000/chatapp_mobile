import React, { PureComponent } from 'react';
import { Text, StyleSheet, TouchableWithoutFeedback, View, GestureResponderEvent, Animated, Easing, StyleProp, ViewStyle, TextStyle } from 'react-native';


interface props {
    onPress: () => void;
    title: string | React.ReactNode;
    type: "contained" | "outlined" | "text";
    rippleColor?: string;
}

interface state {
    locationX: number;
    locationY: number;
}

export default class AnimatedButton extends PureComponent<props, state> {
    state = {
        locationX: 0,
        locationY: 0
    }

    locationX: number = 0;
    locationY: number = 0;

    scaleSize: Animated.Value = new Animated.Value(0.01);
    opacitySize: Animated.Value = new Animated.Value(0.18);

    onPressIn(ev:GestureResponderEvent) {
        this.setState({
            locationX: ev.nativeEvent.locationX,
            locationY: ev.nativeEvent.locationY
        });
        this.locationX = ev.nativeEvent.locationX;
        this.locationY = ev.nativeEvent.locationY;
        
        Animated.timing(this.scaleSize, {
            toValue: 1,
            easing: Easing.bezier(0, 0, .2, 1),
            duration: 300,
            useNativeDriver: true
        }).start();
    }

    onPressOut(ev:GestureResponderEvent) {
        Animated.timing(this.opacitySize, {
            toValue: 0,
            useNativeDriver: true
        }).start(() => {
            this.scaleSize.setValue(0.01);
            this.opacitySize.setValue(0.18);
        })
    }

    renderAnimationCircle() {
        return (
            <Animated.View style={{
                position: "absolute",
                left: -55,
                top: -230,
                width: 450,
                height: 450,
                borderRadius: 150,
                opacity: this.opacitySize,
                transform: [{scale: this.scaleSize}],
                backgroundColor: this.props.rippleColor ? this.props.rippleColor : "#fff"
            }} />
                    
        )
    }

    getStyleByType() {
        let styleButton;
        let styleTextButton;

        switch (this.props.type) {
            case "contained":
                styleButton = {
                    backgroundColor: "#0078FF",
                }
                styleTextButton = {
                    color: "#fff"
                }
                break;
            
            case "outlined":
                styleButton = {
                    opacity: 1,
                    borderColor: "#0078FF",
                    borderWidth: 1
                }
                styleTextButton = {
                    color: "#0078FF"
                }
                break;
            
            case "text":
                styleButton = {
                    opacity: 1,
                }
                styleTextButton = {
                    color: "#0078FF"
                }
                break;

            default:
                break;
        }

        return {styleButton, styleTextButton}
    }

    render() {
        const styleByType = this.getStyleByType();

        return (
            <TouchableWithoutFeedback 
                onPress={this.props.onPress}
                onPressIn={this.onPressIn.bind(this)}
                onPressOut={this.onPressOut.bind(this)}
            >
                <View style={{
                        ...styles.buttonStyle, 
                        ...styleByType.styleButton
                    }}>
                    {this.renderAnimationCircle.bind(this)()}
                    <Text style={{
                        ...styles.textButton,
                        ...styleByType.styleTextButton
                    }}>
                        {this.props.title}
                    </Text>
                </View>
            </TouchableWithoutFeedback>
        )
    }
}

const styles = StyleSheet.create({
    buttonStyle: {
        height: 50,
        borderRadius: 5,
        justifyContent: "center",
        alignItems: "center",
        position: "relative",
        overflow: "hidden",
        paddingHorizontal: 20
    },
    textButton: {
        fontSize: 18
    }
});
