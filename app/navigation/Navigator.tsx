import ChatListScreen from "../screens/ChatList";
import ChatScreen from "../screens/ChatScreen";
import LoginScreen from "../screens/Login";
import LoadingScreen from "../screens/loading.screen";
import SignUpScreen from "../screens/SignUp";
import React from 'react';

import { 
    NavigationContainer, createSwitchNavigator, createAppContainer, createBottomTabNavigator
} from "react-navigation";
import { AntDesign, Ionicons } from '@expo/vector-icons';
import { createStackNavigator } from "react-navigation-stack";
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import ContactList from "../screens/ContactList";



const ChatStack:NavigationContainer = createStackNavigator({ 
    ChatListScreen: {
        screen: ChatListScreen
    }, 
    ChatScreen: {
        screen: ChatScreen
    } 
});

ChatStack.navigationOptions = ({navigation}) => {
    return {
        tabBarVisible: navigation.state.routes[navigation.state.index].routeName !== "ChatScreen"
    }
}

const HomeStack:NavigationContainer = createMaterialBottomTabNavigator(
    {
        ChatStack: {
            screen: ChatStack,
            navigationOptions: {
                title: "",
                tabBarIcon: (props) => (
                    <AntDesign name="message1" style={{paddingTop: 5}} color={props.tintColor} size={24} />
                )
            }
        },
        ContactList: {
            screen: ContactList,
            navigationOptions: {
                tabBarIcon: (props) => (
                    <Ionicons name="ios-contacts" style={{paddingTop: 5}} color={props.tintColor} size={24} />
                ),
                title: "",
            }
        }
    },
    {
        initialRouteName: 'ChatStack',
        barStyle: {backgroundColor: "#fff", height: 50, },
        activeColor: "#0078FF",
        inactiveColor: "#AEBFCE"
    }
)

const LoginStack:NavigationContainer = createStackNavigator({ 
    LoginScreen, SignUpScreen, 
}, {headerMode: "none"});

const RootSwitch:NavigationContainer = createSwitchNavigator(
    { LoadingScreen, LoginStack, HomeStack },
    { initialRouteName: "LoginStack" }
);

const AppContainer:NavigationContainer = createAppContainer(RootSwitch);

export default AppContainer;