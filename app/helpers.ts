import config from "./configure";
import { AsyncStorage } from "react-native";

async function postRequest(url:string, data:any) {
    let headers:Headers = new Headers({ "Content-Type": "application/json" });
    const token:string = await AsyncStorage.getItem("token");

    if (token) headers.append("Authorization", `Bearer ${token}`);
    
    return fetch(config.urlServer + url, {
        method: "POST",
        headers: headers,
        body: JSON.stringify(data)
    }).then((res) => {
        if (res.status === 200) {
            return res.json();
        } else {
            throw new Error(`Status code: ${res.status}`)
        }
    });
}

async function getRequest(url:string, params:any) {
    let url_fetch = config.urlServer + url + "?";
    Object.keys(params).forEach((key:string) => url_fetch += `${key}=${params[key]}&`);
    url_fetch = url_fetch.slice(0, -1);

    let headers:Headers = new Headers({ "Content-Type": "application/json" });
    const token:string = await AsyncStorage.getItem("token");

    if (token) headers.append("Authorization", `Bearer ${token}`);
    
    return fetch(url_fetch, {
        method: "GET",
        headers: headers,
    }).then((res) => {
        if (res.status === 200) {
            return res.json();
        } else {
            throw new Error(`Status code: ${res.status}`)
        }
    });
}


export {postRequest, getRequest};
