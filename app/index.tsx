import React, { Component } from 'react';
import Navigator from "./navigation/Navigator";
import configureStore from './store';
import { Provider } from 'react-redux';

const store = configureStore();

class ChatApp extends Component {
    render() {
        return (
            <Provider store={store}>
                <Navigator />
            </Provider>
        );
    }
}

export default ChatApp;
