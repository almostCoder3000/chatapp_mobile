export interface IUser {
    _id: string;
    login: string;
    firstName: string;
    secondName: string;
}

export interface UserState {
    fetching: boolean;
    contacts: IUser[];
    me: IUser;
    error: Object | undefined;
}


export const START_FETCH = "START_FETCH";
export const FETCH_ERROR = "FETCH_ERROR";

// success
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const GET_USER_SUCCESS = "GET_USER_SUCCESS";
export const VK_AUTH_SUCCESS = "VK_AUTH_SUCCESS";
export const GET_ME_SUCCESS = "GET_ME_SUCCESS";
export const REGISTER_USER_SUCCESS = "REGISTER_USER_SUCCESS";
export const RESET_REGISTER_FLAG = "RESET_REGISTER_FLAG";


export interface StartFetchAction {
    type: typeof START_FETCH;
    payload?: UserState
}

interface FetchActionError {
    type: typeof FETCH_ERROR;
    payload?: UserState;
    error?: any;
}


interface AuthActionSuccess {
    type: typeof AUTH_SUCCESS;
    payload: IUser;
}
interface GetUserSuccess {
    type: typeof GET_USER_SUCCESS;
    payload: IUser[];
}
interface GetMeSuccess {
    type: typeof GET_ME_SUCCESS;
    payload: IUser;
}
interface VkAuthActionSuccess {
    type: typeof VK_AUTH_SUCCESS;
    payload: IUser
}
interface RegisterUserSuccess {
    type: typeof REGISTER_USER_SUCCESS;
    payload: {success: boolean; error?: string;}
}


export type UserActionTypes = StartFetchAction 
                              | FetchActionError
                              | GetMeSuccess
                              | AuthActionSuccess
                              | RegisterUserSuccess
                              | VkAuthActionSuccess
                              | GetUserSuccess;