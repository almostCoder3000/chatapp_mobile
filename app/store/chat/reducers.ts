import { 
    ChatState, ChatActionTypes, START_FETCH, 
    FETCH_ERROR, GET_CHAT_LIST_SUCCESS, GET_CURRENT_CHAT_SUCCESS, GET_MESSAGES_SUCCESS, SEND_MESSAGE_SUCCESS, CLEAR_CHAT_SCREEN 
} from "./types";

const initialState: ChatState = {
    fetching: false,
    chat_list: [],
    current_chat: { _id: "", messages: [], users: [], contact: {firstName: "", secondName: "", login: "", _id: ""} },
    error: ""
};

export function chatReducer( 
    state = initialState, 
    action: ChatActionTypes
): ChatState {

    switch (action.type) {
    case START_FETCH:
        return {
            ...state,
            fetching: true
        };

    case CLEAR_CHAT_SCREEN:
        return {
            ...state,
            current_chat: {
                ...state.current_chat,
                messages: []
            }
        }

    case GET_CHAT_LIST_SUCCESS:
        return {
            ...state,
            chat_list: action.payload,
            fetching: false
        };

    case GET_MESSAGES_SUCCESS:
        return {
            ...state,
            fetching: false,
            current_chat: {
                ...state.current_chat,
                messages: action.payload
            }
        }

    case SEND_MESSAGE_SUCCESS:
        return {
            ...state,
            fetching: false,
            current_chat: {
                ...state.current_chat,
                messages: state.current_chat.messages.concat(action.payload)
            }
        }

    case GET_CURRENT_CHAT_SUCCESS:
        return {
            ...state,
            fetching: false,
            current_chat: action.payload
        }

    case FETCH_ERROR:
        return {
            ...state,
            fetching: false,
            error: action.error
        };


    default:
        return state;
    }
}